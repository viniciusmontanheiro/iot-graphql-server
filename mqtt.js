import mosca from "mosca";

const pubsubSettings = {
  //using ascoltatore
  type: "mongo",
  url: "mongodb://localhost:27017/iot",
  pubsubCollection: "ascoltatore",
  mongo: {}
};

const settings = {
  port: 1883,
  backend: pubsubSettings
};

const setup = () => {
  console.log("Mosca server is up and running");
}

const mqttServerStart = () => {
  const server = new mosca.Server(settings);
  server.on("ready", setup);
  // fired when a message is published
  server.on("published", function(packet, client) {
    console.log("Published", packet);
    console.log("Client", client);
  });
  // fired when a client connects
  server.on("clientConnected", function(client) {
    console.log("Client Connected:", client.id);
  });

  // fired when a client disconnects
  server.on("clientDisconnected", function(client) {
    console.log("Client Disconnected:", client.id);
  });
};

export default mqttServerStart;
