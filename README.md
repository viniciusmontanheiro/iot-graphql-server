## Running with Docker
You can use `docker-compose up` to  run an instance of GraphQL-MongoDB Server and MongoDB locally.

```bash
docker-compose up
```

## Requirements

* [Node.js](http://nodejs.org/)
* [MongoDB](https://www.mongodb.org/) 

