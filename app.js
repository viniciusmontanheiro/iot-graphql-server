import express from 'express';
import http from 'http';
import graphqlHTTP from 'express-graphql';
import mongoose from 'mongoose';

import schema from './graphql';
import mqttServerStart from './mqtt';

const app = express();

// GraphqQL server route
app.use('/graphql', graphqlHTTP(req => ({
  schema,
  pretty: true,
  graphiql: true
})));

// Creating mqtt server
mqttServerStart(); 

// Connect mongo database
const db = mongoose.createConnection('mongodb://localhost/iot');

db.on('error', function(err){
  if(err) throw err;
});

db.once('open', function callback () {
  console.info('Mongo db connected successfully');
});

const server = http.createServer(app);
// start server
server.listen(3000, function(err) {
  if (err) {
    console.error(new Error(':-( Server DOWN!!'), err);
  }
  console.log('Listening at port', 3000);
});


